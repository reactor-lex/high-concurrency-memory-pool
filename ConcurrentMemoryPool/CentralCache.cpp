#include "CentralCache.h"
#include "PageCache.h"
CentralCache CentralCache::_sInst;//单例对象的定义
/**
  * @brief  从中心缓存获取一定数量的对象给thread cache
  * @param  start：申请对象的起始地址
  * @param  end：申请对象的结束地址
  * @param  batchNum：通过调节算法得出的中央缓存应该给线程缓存的对象个数
  * @param  size：线程缓存申请的单个对象大小
  * @retval 中央缓存实际给的对象个数，因为当前页的资源对象可能不足了。
  */
size_t CentralCache::FetchRangeObj(void*& start, void*& end, size_t batchNum, size_t size)
{
	size_t index = SizeClass::Index(size); //计算线程申请的内存对象向上对齐后位于哪一个桶
	_spanLists[index]._mtx.lock();//线程进来先上桶锁
	Span* span = CentralCache::GetOneSpan(_spanLists[index], size);//获取非空的页
	//从该非空页中拿走对象交给threan cache
	assert(span);
	assert(span->_freeList);
	start = span->_freeList;
	end = start;
	size_t i = 0;
	while (i < batchNum - 1 && NextObj(end) != nullptr)
	{
		++i;
		end = NextObj(end);
	}
	span->_freeList= NextObj(end);
	NextObj(end) = nullptr;
	//测试
	//void* cur = start;
	//int m = 0;
	//while (cur)
	//{
	//	cur = NextObj(cur);
	//	++m;
	//}
	//if (m != i+1)
	//{
	//	int x = 0;//断点
	//}
	_spanLists[index]._mtx.unlock();
	return i + 1;
}
/**
  * @brief  从SpanList或者页缓存获取一个非空的span
  * @param  list：中央缓存当前桶的SpanList
  * @param  size：需要申请的对象个数
  * @retval 返回非空的页地址
  */
Span* CentralCache::GetOneSpan(SpanList& list, size_t size)
{
	Span* it = list.Begin();  
	while (it != list.End())
	{
		if (it->_freeList != nullptr)
		{
			//说明central cache可以满足本次资源申请
			return it;
		}
		it = it->_next;
	}
	//先把central cacha的桶锁解除，这样如果其他线程释放内存，将不会被阻塞
	list._mtx.unlock();
	//走到这里，就要找页缓存要空间了
	PageCache::GetInStance()->_pageMtx.lock();//page cache整体的一把大锁
	Span* span=PageCache::GetInStance()->NewSpan(SizeClass::NumMovePage(size));//要空间
	span->_isUse = true;//当前内存对象状态修改为正在使用
	span->_objSize = size;//将需要申请的对象的szie保存起来，后续释放时就不用传地址了
	PageCache::GetInStance()->_pageMtx.unlock();
	//对获取的span进行切分，不需要加锁，因为其他线程访问不到这个span
	//从页缓存要到页后，切分成小块内存
	char* startAddress = (char*)(span->_pageId << PAGE_SHIFT);//根据页号回推页的起始地址虚拟地址
	size_t bytes = span->_n << PAGE_SHIFT;//计算大块内存的字节数
	char* endAddress = startAddress + bytes;//这里就是为什么用char*而不是void*，因为void*没办法移动
	//将小块内存尾插至central cache的page中
	span->_freeList = startAddress;//自由链表指向第一块内存对象,作为头结点
	startAddress += size;//进行地址偏移，准备继续尾插至自由链表
	void* tail = span->_freeList;
	int i = 1;//用于观察大块内存被切分了几块
	while (startAddress < endAddress)
	{
		++i;
		NextObj(tail) = startAddress;
		tail = NextObj(tail);
		startAddress += size;
	}
	NextObj(tail) = nullptr;//尾结点置空
	//测试。条件断点
	//意思死循环，可以中断程序（调试->全部中断），程序会在正在运行的地方停下来
	//void* cur = span->_freeList;
	//int m = 0;
	//while (cur)
	//{
	//	cur = NextObj(cur);
	//	++m;
	//}
	//if (m != (bytes / size))
	//{
	//	int x = 0;//断点
	//}
	//切好span以后，需要把span挂到桶里面去的时候，再进行加锁
	list._mtx.lock();
	//将span插入到SpanList& list中
	list.PushFront(span);//头插进SpanList
	return span;
}
/**
  * @brief  将线程缓存归还的内存页挂载至对应的中心缓存的哈希桶中的spanList中，
  *			注意，线程缓存所归还的缓存不一定会放在中心缓存哈希桶的同一个span中
  * @param  start：线程缓存所归还的内存对象的起始位置
  * @param  size：所归还的单个内存对象的大小，用于计算其位于中心缓存的哪一个桶
  * @retval 返回非空的页地址
  */
void CentralCache::ReleaseListToSpans(void* start, size_t size)
{
	size_t index = SizeClass::Index(size);//计算归还对象位于中心缓存的哪一个哈希桶
	_spanLists[index]._mtx.lock();//加上桶锁
	while (start != nullptr)
	{
		void* next = NextObj(start);//记录下一个位置
		Span* span = PageCache::GetInStance()->MapObjectToSpan(start);//获取对象->page的映射地址
		//将内存块插入进中央缓存的
		NextObj(start) = span->_freeList;
		span->_freeList = start;
		//当前span被分出去的内存对象，减到0说明当前span内存都回来了,这个span就可以再归还给页缓存
		span->_useCount--;
		if (0 == span->_useCount)
		{
			_spanLists->Erase(span);//拿出这个span
			span->_freeList = span->_next = span->_prev = nullptr;//清空span的所有指针
			_spanLists[index]._mtx.unlock();//解除桶锁
			PageCache::GetInStance()->_pageMtx.lock();//加上页缓存的大锁
			PageCache::GetInStance()->ReleaseSpanToPageCache(span);//页缓存回收span
			PageCache::GetInStance()->_pageMtx.unlock();//解除页缓存的大锁
			_spanLists[index]._mtx.lock();//加上桶锁
		}
		start = next;
	}
	_spanLists[index]._mtx.unlock();//解除桶锁

}
