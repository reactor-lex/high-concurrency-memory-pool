#pragma once
//这个头文件是封装一层，给外部调用的
#include "Common.h"
#include "ThreadCache.h"
#include "PageCache.h"
#include "ObjectPool.h"
/**
  * @brief  1、如果当前线程没有独立的线程缓存，则创建线程缓存。
			2、线程通过该函数去各自的线程缓存申请内存块
  * @param  size：线程缓存需要申请的内存块大小
  * @retval 返回申请到的内存对象的地址
  */
static void* ConcurrentAlloc(size_t size)//加上static，防止该头文件被源文件重复包含导致函数重定义
{
	if (size > MAX_BYTES)//线程单次申请的缓存大于等于32页小于等于128页或者大于128页
	{
		size_t alignSize = SizeClass::RoundUp(size);//用于计算线程申请的内存对象向上对齐后的大小
		size_t kPage = alignSize >> PAGE_SHIFT;//将向上对齐后的大小
		PageCache::GetInStance()->_pageMtx.lock();//向page cache要内存记得加一下锁
		Span* span = PageCache::GetInStance()->NewSpan(kPage);//从page cache获取kPage页的span
		span->_objSize = size;//获取归还的span的size，这样外部就不用显示的传size了
		PageCache::GetInStance()->_pageMtx.unlock();
		void* ptr = (void*)(span->_pageId << PAGE_SHIFT);//将申请到的页id转换为虚拟地址
		return ptr;
	}
	else
	{
		//为每个线程开辟独立的thread cache
		if (nullptr == pTLSThreadCache)
		{
			static ObjectPool<ThreadCache> tcPool;
			//pTLSThreadCache = new ThreadCache;
			pTLSThreadCache = tcPool.New();
		}
		//cout << std::this_thread::get_id() << ":" << pTLSThreadCache << endl;
		return pTLSThreadCache->Allocate(size);
	}
}
static void ConcurrentFree(void* ptr)//外部调用该函数，释放内存块
{
	Span* span = PageCache::GetInStance()->MapObjectToSpan(ptr);//返回从对象到span的映射
	size_t size = span->_objSize;//获取归还的span的size，这样外部就不用显示的传size了
	if (size > MAX_BYTES)//大于32页
	{
		PageCache::GetInStance()->_pageMtx.lock();//向page cache要内存记得加一下锁
		PageCache::GetInStance()->ReleaseSpanToPageCache(span);//对span前后的页，尝试进行合并
		PageCache::GetInStance()->_pageMtx.unlock();
	}
	else
	{
		assert(pTLSThreadCache);
		pTLSThreadCache->Deallocate(ptr, size);
	}
}