#pragma once
#include "Common.h"
#include "ObjectPool.h"
#include "PageMap.h"
class PageCache
{
public:
	//使用偷家函数获取静态单例对象，加static是因为静态方法无需对象即可调用
	static PageCache* GetInStance()
	{
		return &_sInstan;
	}
	Span* NewSpan(size_t k);//获取k页的span
	//获取从对象到span的映射
	Span* MapObjectToSpan(void* obj);
	//页缓存回收中央缓存的span，并合并相邻的span
	void ReleaseSpanToPageCache(Span* span);
public:
	std::mutex _pageMtx;//页缓存整体的一把锁
private:
	SpanList _spanLists[NPAGES];//页缓存最大哈希桶中的页大小
private:
	static PageCache _sInstan;//页缓存单例对象
	//std::unordered_map<PAGE_ID, Span*> _idSpanMap;//用于映射page的id和span的位置关系，便于中央缓存、页缓存确定回收内存对象应该位于哪一个page
	TCMalloc_PageMap1<32 - PAGE_SHIFT> _idSpanMap;//一级基数树替代unordered_map

	ObjectPool<Span> _spanPool;//定长内存池对象
private:
	//私有构造+禁用拷贝和赋值运算符重载
	PageCache   ()
	{}
	PageCache(const PageCache&) = delete;
	PageCache& operator=(const PageCache&) = delete;
};
