#pragma once
#include "Common.h"

//template <size_t N>//N代表每个内存块的大小
//class ObjectPool//定长内存池
//{};
template <class T>//T代表内存对象，每个内存对象的大小是一样的，表示内存块的大小
class ObjectPool//定长内存池
{
public:
	T* New()//内存池单次资源申请
	{
		T* obj = nullptr;
		//申请资源时优先重复利用已归还的资源块
		if (_freeList != nullptr)
		{
			obj = (T*)_freeList;
			_freeList = *(void**)_freeList;
		}
		else
		{
			if (_remainBytes < sizeof(T))//当剩余内存小于一个T对象时，重新开辟一块新内存池
			{
				_remainBytes = 128 * 1024;
				_memory = (char*)SystemAlloc(_remainBytes>>13);//128字节换算成每页8kb,得16页
				if (nullptr == _memory)
				{
					throw std::bad_alloc();
				}
			}
			//内存池单次资源申请
			obj = (T*)_memory;
			size_t objSize = sizeof(T) < sizeof(void*) ? sizeof(void*) : sizeof(T);
			_memory += objSize;
			_remainBytes -= objSize;
		}
		//使用定位new，显式调用T的构造函数初始化构造函数
		new(obj)T;
		return obj;
	}
	void Delete(T* obj)
	{
		obj->~T();//显式调用obj的析构函数，清理对象
		//归还资源块时，进行单链表的头插
		*(void**)obj = _freeList;//找到资源块的头4/8个字节，取决于32位还是64位机器
		_freeList = obj;
	}
private:
	char* _memory = nullptr;//内存池的起始地址
	size_t _remainBytes = 0;//内存池剩余内存
	void* _freeList = nullptr;//指向归还资源块的单链表头指针
};
struct TreeNode
{
	int val;
	TreeNode* _left;
	TreeNode* _right;
	TreeNode()
		:val(0)
		,_left(nullptr)
		,_right(nullptr)
	{}
};
//void TestObjectPool()//测试代码
//{
//	//申请释放的轮次
//	const size_t Rounds = 5;
//	//每轮申请释放多少次
//	const size_t N = 10000;
//	std::vector<TreeNode*> v1;
//	v1.reserve(N);
//	size_t begin1 = clock();
//	for (size_t j = 0; j < Rounds; ++j)
//	{
//		for (int i = 0; i < N; ++i)
//		{
//			v1.push_back(new TreeNode);
//		}
//		for (int i = 0; i < N; ++i)
//		{
//			delete v1[i];
//		}
//		v1.clear();
//	}
//	size_t end1 = clock();
//	std::vector<TreeNode*> v2;
//	v2.reserve(N);
//	ObjectPool<TreeNode> TNPool;
//	size_t begin2 = clock();
//	for (size_t j = 0; j < Rounds; ++j)
//	{
//		for (int i = 0; i < N; ++i)
//		{
//			v2.push_back(TNPool.New());
//		}
//		for (int i = 0; i < N; ++i)
//		{
//			TNPool.Delete(v2[i]);
//		}
//		v2.clear();
//	}
//	size_t end2 = clock();
//	cout << "new cost time:" << end1 - begin1 << endl;
//	cout << "object pool cost time:" << end2 - begin2 << endl;
//}



