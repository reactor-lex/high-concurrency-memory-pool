#pragma once
#include "Common.h"
//单例饿汉模式
class CentralCache
{
public:
	//使用偷家函数获取静态单例对象，加static是因为静态方法无需对象即可调用
	static CentralCache* GetInStance()
	{
		return &_sInst;
	}
	size_t FetchRangeObj(void*& start, void*& end, size_t n, size_t size);
	
	Span* GetOneSpan(SpanList& list, size_t byte_size);
	// 将一定数量的对象释放到span跨度
	void ReleaseListToSpans(void* start, size_t byte_size);
private:
	SpanList _spanLists[NFREE_LIST];//208个桶
private:
	static CentralCache _sInst;//静态单例对象(不要在.h文件定义，因为源文件包含了该头文件即可看到单例对象)
	//构造函数私有+禁用拷贝构造和赋值
	CentralCache()
	{}
	CentralCache(const CentralCache&) = delete;
	CentralCache& operator=(const CentralCache&) = delete;
};
