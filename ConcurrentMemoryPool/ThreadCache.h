#pragma once
#include "Common.h"

class ThreadCache
{
public:
	//申请和释放对象
	void* Allocate(size_t size);
	void Deallocate(void* ptr, size_t size);
	// 从中央缓存获取对象
	void* FetchFromCentralCache(size_t index, size_t size);//index：计算位于哪一个桶；size:对齐之后的内存块大小
	// 释放对象，链表过长时，回收内存到中心缓存
	void ListTooLong(FreeList& list, size_t size);
private:
	FreeList _freeList[NFREE_LIST];//挂载208个哈希桶的哈希表
};

//每个线程都有一份自己的pTLSThreadCache
static _declspec(thread)ThreadCache* pTLSThreadCache = nullptr;//pTLSThreadCache:指向Threadcache对象的指针
