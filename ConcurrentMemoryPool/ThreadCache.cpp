#include "ThreadCache.h"
#include "CentralCache.h"
/**
  * @brief  线程缓存从中央缓存中获取内存
  * @param  index：哈希桶的下标
  * @param  size：线程缓存需要申请的内存块大小
  * @retval 返回获得的内存对象的起始地址
  */
void* ThreadCache::FetchFromCentralCache(size_t index, size_t size)
{
	//慢开始调节算法
	size_t batchNum = min(_freeList[index].MaxSize(),SizeClass::NumMoveSize(size));
	if (batchNum == _freeList[index].MaxSize())
	{
		_freeList[index].MaxSize() += 1;
	}
	void* start = nullptr;
	void* end = nullptr;
	size_t actualNum=CentralCache::GetInStance()->FetchRangeObj(start,end, batchNum,size);
	assert(actualNum >= 1);//至少得有一个
	if (1 == actualNum)
	{
		assert(start == end);
	}
	else
	{
		_freeList[index].PushRange(NextObj(start), end, actualNum - 1);
	}
	return start;
}
/**
  * @brief  线程从线程缓存中申请对象
  * @param  bytes：线程缓存需要申请的内存块大小
  * @retval 返回申请到的内存对象的地址
  */
void* ThreadCache::Allocate(size_t bytes)
{
	assert(bytes <= MAX_BYTES);
	size_t alignNum = SizeClass::RoundUp(bytes);//对齐之后的内存块大小
	size_t index = SizeClass::Index(bytes);//计算位于哪一个桶
	if (!_freeList[index].Empty())//去对应的哈希桶中的自由链表中申请资源块
	{
		return _freeList[index].Pop();
	}
	else//如果对应的自由链表已经没有资源块了，那就要去中央缓存申请资源了
	{
		return FetchFromCentralCache(index, alignNum);//index：计算位于哪一个桶；size:对齐之后的内存块大小
	}
}
/**
  * @brief  线程释放对象至线程缓存
  * @param  ptr：所释放的对象的地址
  * @param  size：所释放的对象大小
  * @retval 无返回值
  */
void ThreadCache::Deallocate(void* ptr, size_t size)
{
	assert(ptr);
	assert(size <= MAX_BYTES);
	size_t index = SizeClass::Index(size);//算出位于几号桶
	//头插
	_freeList[index].Push(ptr);
	//当链表长度大于一次批量申请的内存时就开始执行回收，归还一段内存给中央缓存 
	if (_freeList[index].Size() >= _freeList[index].MaxSize())
	{
		ListTooLong(_freeList[index], size);
	}
}
/**
  * @brief  线程缓存链表过长时，回收内存至中心缓存
  * @param  list：某个哈希桶中过长链表的地址
  * @param  size：回收对象的个数
  * @retval 无返回值
  */
void ThreadCache::ListTooLong(FreeList& list, size_t size)
{
	void* start = nullptr;
	void* end = nullptr;
	list.PopRange(start, end, list.MaxSize());//从线程缓存裁剪一段内存出来
	CentralCache::GetInStance()->ReleaseListToSpans(start, size);//将内存还给中心缓存
}
