#include "PageCache.h"
PageCache PageCache::_sInstan;
/**
  * @brief  从page cache获取k页的span，一共分为三种情况
  *			1、小于32页（线程走完三级缓存才走到这里）
  *			2、大于等于32页小于128页（线程将直接问page cache的该函数要内存）
  *			3、大于128页（向堆要）
  * @param  k：页数
  * @retval 返回获取到的span的地址
  */
Span* PageCache::NewSpan(size_t k)
{
	assert(k > 0);
	if (k > NPAGES-1)//大于128页
	{
		void* ptr = SystemAlloc(k);//向堆申请空间
		//Span* span = new Span;
		Span* span = _spanPool.New();
		span->_pageId = (PAGE_ID)ptr >> PAGE_SHIFT;//将申请的空间地址转换为页id 
		span->_n = k;//页数是k
		//_idSpanMap[span->_pageId] = span;//建立页id与span*的映射关系
		_idSpanMap.set(span->_pageId, span);//建立页id与span*的映射关系
		return span;
	}
	//先看一下映射桶有没有对象，有的话直接拿走
	if (!_spanLists[k].Empty())
	{
		Span* kSpan= _spanLists[k].PopFront();
		//切出来k页给中央缓存，遍历，对每一页建立id和span的映射关系，方便中央缓存回收内存块时，查找对应的span
		for (PAGE_ID i = 0; i < kSpan->_n; ++i)
		{
			//_idSpanMap[kSpan->_pageId + i] = kSpan;
			_idSpanMap.set(kSpan->_pageId + i, kSpan);
		}
		return kSpan;
	}
	//检查一下后面的哈希桶里有没有span
	for (auto i = k+1; i < NPAGES; ++i)
	{
		if (!_spanLists[i].Empty())
		{
			//拿出来切分,切分成k页和i-k页，k页的span给central cache，i-k页的span挂到i-k的哈希桶
			Span* nSpan = _spanLists[i].PopFront();
			//Span* kSpan = new Span;
			Span* kSpan = _spanPool.New();
			//对nSpan进行头切k页
			kSpan->_pageId = nSpan->_pageId;
			kSpan->_n = k;
			nSpan->_pageId += k;
			nSpan->_n -= k;
			//将剩余对象头插进哈希桶
			_spanLists[nSpan->_n].PushFront(nSpan);
			//将剩余对象（nSpan）的首尾id与span进行映射，便于page cache回收内存时进行合并查找
			//_idSpanMap[nSpan->_pageId] = nSpan;//首
			_idSpanMap.set(nSpan->_pageId, nSpan);
			//_idSpanMap[nSpan->_pageId+nSpan->_n-1] = nSpan;//尾
			_idSpanMap.set(nSpan->_pageId + nSpan->_n - 1, nSpan);
			//切出来k页给中央缓存，遍历，对每一页建立id和span的映射关系，方便中央缓存回收内存块时，查找对应的span
			for (PAGE_ID i = 0; i < kSpan->_n; ++i)
			{
				//_idSpanMap[kSpan->_pageId + i]=kSpan;
				_idSpanMap.set(kSpan->_pageId + i, kSpan);
			}
			return kSpan;
		}
	}
	//找遍了还是没有，这时，page cache就要找堆要一块128页的span
	//Span* bigSpan = new Span;
	Span* bigSpan = _spanPool.New();
	void* ptr = SystemAlloc(NPAGES - 1);
	bigSpan->_pageId = (PAGE_ID)ptr >> PAGE_SHIFT;
	bigSpan->_n = NPAGES - 1;
	//将bigSpan插入到对应的桶中
	_spanLists[bigSpan->_n].PushFront(bigSpan);
	return PageCache::NewSpan(k);//递归调用自己，让下一个栈帧处理切分、挂起逻辑
}
/**
  * @brief  获取从span*到span的映射
  * @param  obj：内存对象的地址
  * @retval 返回从对象到span的映射
  */
Span* PageCache::MapObjectToSpan(void* obj)
{
	PAGE_ID id = ((PAGE_ID)obj >> PAGE_SHIFT);//将span*由地址转换为页号
	//std::unique_lock<std::mutex> lock(_pageMtx);//RAII的锁
	//std::unordered_map<PAGE_ID, Span*>::iterator ret = _idSpanMap.find(id);//查找unordered_map中是否存在该id，返回迭代器
	//if (ret != _idSpanMap.end())
	//{
	//	return ret->second;//返回span的地址
	//}
	//else//如果找不到，你的代码肯定是有问题的
	//{
	//	assert(false);
	//	return nullptr;
	//}
	auto ret = (Span*)_idSpanMap.get(id);
	assert(ret != nullptr);
	return ret;
}
/**
  * @brief  页缓存回收中央缓存的span，并合并相邻的span
  *			如果是大于等于32页小于等于128页，也会来尝试一下页缓存能否合并
  * @param  span：span的地址
  * @retval 无返回值
  */
void PageCache::ReleaseSpanToPageCache(Span* span)
{
	if (span->_n > NPAGES - 1)//如果n大于128页
	{
		void* ptr = (void*)(span->_pageId << PAGE_SHIFT);//将页id转换为虚拟地址
		SystemFree(ptr);//释放内存
		//delete(span);//span是new出来的
		_spanPool.Delete(span);
	}
	else
	{
		//对span前后的页，尝试进行合并
	//向前合并
		while (1)
		{
			PAGE_ID prevId = span->_pageId - 1;//先判断一下前一个span是否可以被回收
			//auto ret = _idSpanMap.find(prevId);
			//if (ret == _idSpanMap.end())//说明ret位置的span不可被回收
			//{
			//	break;
			//}
			auto ret = (Span*)_idSpanMap.get(prevId);
			if (ret == nullptr)
			{
				break;
			}
			Span* prevSpan = ret;
			if (prevSpan->_isUse == true)//说明该span正在中央缓存，无法回收
			{
				break;
			}
			if (prevSpan->_n + span->_n > NPAGES - 1)//超过108页
			{
				break;
			}
			//进行合并
			span->_pageId = prevSpan->_pageId;
			span->_n += prevSpan->_n;
			_spanLists[prevSpan->_n].Erase(prevSpan);
			//delete prevSpan;
			_spanPool.Delete(prevSpan);
		}
		//向后合并
		while (1)
		{
			PAGE_ID nextId = span->_pageId + span->_n;//先判断一下前一个span是否可以被回收
			//auto ret = _idSpanMap.find(nextId);
			//if (ret == _idSpanMap.end())//说明ret位置的span不可被回收
			//{
			//	break;
			//}
			auto ret = (Span*)_idSpanMap.get(nextId);
			if (ret == nullptr)
			{
				break;
			}
			Span* nextSpan = ret;
			if (nextSpan->_isUse == true)//说明该span正在中央缓存，无法回收
			{
				break;
			}
			if (nextSpan->_n + span->_n > NPAGES - 1)//超过108页
			{
				break;
			}
			//进行合并
			span->_n += nextSpan->_n;
			_spanLists[nextSpan->_n].Erase(nextSpan);
			//delete nextSpan;
			_spanPool.Delete(nextSpan);
		}
		//走到这里，将span头插至对应页缓存中的哈希桶中
		_spanLists[span->_n].PushFront(span);//
		span->_isUse = false;//更改span的回收状态
		//_idSpanMap[span->_pageId] = span;//建立映射
		_idSpanMap.set(span->_pageId, span);
		//_idSpanMap[span->_pageId + span->_n - 1] = span;//建立映射
		_idSpanMap.set(span->_pageId + span->_n - 1, span);
	}
}
